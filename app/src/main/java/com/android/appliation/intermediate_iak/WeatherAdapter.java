package com.android.appliation.intermediate_iak;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.appliation.intermediate_iak.model.model_movie;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adesanto on 11/4/17.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {
    List<model_movie> weatherList;
    private Context context;

    public WeatherAdapter(Context context, List<model_movie> weatherListData) {
        weatherList = weatherListData;
        this.context = context;
    }

    private static final String TAG = "WeatherAdapter";

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder panjang list: " + weatherList.size());
        View viewContent = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
        return new WeatherViewHolder(viewContent);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {

//        holder.itemTitle.setText("Cuaca Hari ini tanggal" + position);
        holder.title.setText(weatherList.get(position).getTitle());
        holder.weatherItemDesc.setText(weatherList.get(position).getDesc());
        holder.rateme.setText(weatherList.get(position).getRating());
        holder.date.setText(weatherList.get(position).getRelease());

        Picasso.with(context).load(weatherList.
                get(position).
                getPoster()).
                into(holder.weatherItemImage);
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        @BindView(R.id.item_title)
//        TextView itemTitle;

        @BindView(R.id.weather_item_image)
        ImageView  weatherItemImage;

        @BindView(R.id.weather_item_date)
        TextView  weatherItemDate;

        @BindView(R.id.weather_item_dsc)
        TextView  weatherItemDesc;

        @BindView(R.id.rate)
        TextView rateme;

        @BindView(R.id.date)
        TextView  date;

        @BindView(R.id.title)
        TextView  title;


        public WeatherViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), WeatherDetailActivity.class);

            model_movie weather = weatherList.get(getAdapterPosition());
            String weatherJson = new GsonBuilder().create().toJson(weather);
            Log.d(TAG, "onClick:" + weatherJson);
            intent.putExtra("weather", weatherJson);

            v.getContext().startActivity(intent);
        }
    }
}
