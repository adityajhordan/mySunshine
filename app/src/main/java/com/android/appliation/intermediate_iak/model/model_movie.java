package com.android.appliation.intermediate_iak.model;

import android.media.Rating;

/**
 * Created by adesanto on 11/19/17.
 */

public class model_movie {
    private String title, language, rating, id, count, desc, release, poster, backdrop, popularity;



    public model_movie(String title, String language, String rating, String id, String count, String desc, String release
    , String poster, String backdrop, String popularity){
        this.popularity = popularity;

        this.title = title;
        this.language = language;
        this.rating = rating;
        this.id = id;
        this.count = count;
        this.desc = desc;
        this.release = release;
        this.poster = poster;
        this.backdrop = backdrop;
    }

    public String getPopularity() {
        return popularity;
    }

    public String getTitle() {
        return title;
    }

    public String getLanguage() {
        return language;
    }

    public String getRating() {
        return rating;
    }

    public String getId() {
        return id;
    }

    public String getCount() {
        return count;
    }

    public String getDesc() {
        return desc;
    }

    public String getRelease() {
        String[] separated = release.split("-");
        String dateNow = separated[2]+" - "+ separated[1]+" - "+separated[0];
        return dateNow;
    }

    public String getPoster() {
        String fullUrl2 = "http://image.tmdb.org/t/p/w500"+poster;
        return fullUrl2;
    }

    public  String getBackdrop() {
        String fullUrl =  "http://image.tmdb.org/t/p/w500"+backdrop;
        return fullUrl;
    }
}
