package com.android.appliation.intermediate_iak;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.appliation.intermediate_iak.model.model_movie;
import com.android.appliation.intermediate_iak.model.model_review;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adesanto on 11/11/17.
 */

public class WeatherDetailActivity extends AppCompatActivity {
    private static final String TAG = "WeatherDetailActivity";

    String urlDetail = "https://api.themoviedb.org/3/movie/";
    String urlDetail2 = "/videos?api_key=5a2952737d5c9ffb1a09cbe15f4c142a&language=en-US";
    String reviewUrl ="/reviews?api_key=5a2952737d5c9ffb1a09cbe15f4c142a&language=en-US&page=1";

    @BindView(R.id.imageme)
    ImageView imageDetail;

    @BindView(R.id.juduldetail)
    TextView judulDetail;

    @BindView(R.id.synopsis)
    TextView synopsis;

    @BindView(R.id.vote)
    TextView voteDetail;

    @BindView(R.id.trailer)
    TextView trailerDetail;

    @BindView(R.id.imageTrailer)
    ImageView imgtrDetail;

    @BindView(R.id.review_list)
    RecyclerView reviewlist;

    @BindView(R.id.backButton)
    ImageView backButton;

    private List<model_review> reviewListData;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);

        ButterKnife.bind(this);

        judulDetail.setMovementMethod(new ScrollingMovementMethod());

        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle intent
        String weatherJson = getIntent().getStringExtra("weather");

        //reformat to normal data from JSON
        final model_movie weather = new GsonBuilder().create().fromJson(weatherJson, model_movie.class);
        Log.d(TAG, "Popularity: " + weather.getPopularity());

        voteDetail.setText(weather.getRating());
        synopsis.setText(weather.getDesc());
        judulDetail.setText(weather.getTitle());
        Picasso.with(this).load(weather.getPoster()).into(imageDetail);


        String URL_MOVIE_BY_ID = urlDetail + weather.getId() + urlDetail2 ;
        Log.d(TAG, "getFullUrl API by ID: "+ URL_MOVIE_BY_ID);

        //review section//
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        reviewlist.setLayoutManager(llm);

        reviewListData = new ArrayList<model_review>();

        final ReviewAdapter reviewAdapter = new ReviewAdapter(this, reviewListData);
        reviewlist.setAdapter(reviewAdapter);

        String urlFinalReview = urlDetail + weather.getId() + reviewUrl;
        JsonObjectRequest reviewRequest = new JsonObjectRequest(Request.Method.GET, urlFinalReview, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "reviewResponse: " + response);
                for (int x = 0; x < 20; x++) {

                    try {
                        JSONArray results = response.getJSONArray("results");
                        JSONObject object = results.getJSONObject(x);

                        String author = object.getString("author");
                        String content = object.getString("content");

                        reviewListData.add(new model_review(author, content));

                        reviewAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(getBaseContext()).add(reviewRequest);
        //end review//

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL_MOVIE_BY_ID, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "dataDetailActivity: " + response);

                try {
                    JSONArray result = response.getJSONArray("results");
                    JSONObject object = result.getJSONObject(0);

                    final String key = object.getString("key");

                    review(key, weather.getId());
                    trailerDetail.setText(
                            Html.fromHtml(
                                    "<a href=\"https://www.youtube.com/watch?v="+key+"\">Play Trailer</a> "));
                    trailerDetail.setMovementMethod(LinkMovementMethod.getInstance());

                    imgtrDetail.setOnClickListener(new View.OnClickListener(){

                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse("https://www.youtube.com/watch?v="+key));
                            startActivity(intent);
                        }
                    });

                    Log.d(TAG, "onResponse: " + key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage() );
            }
        });
        Volley.newRequestQueue(getBaseContext()).add(jsonObjectRequest);

    }

    public void swapImg(){
        Picasso.with(this).load("http://i.imgur.com/DvpvklR.png").into(imageDetail);

    }

    public void review(String key, String id){
        Log.d(TAG, "review: "+ key + id);

    }


}
