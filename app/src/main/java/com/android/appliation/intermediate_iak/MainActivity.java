package com.android.appliation.intermediate_iak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.appliation.intermediate_iak.model.model_movie;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    String urlMovieUpComing = "http://api.themoviedb.org/3/movie/upcoming?api_key=5a2952737d5c9ffb1a09cbe15f4c142a";
    String urlMovieTopRated = "http://api.themoviedb.org/3/movie/top_rated?api_key=5a2952737d5c9ffb1a09cbe15f4c142a";
    String urlMovieFavourite = "http://api.themoviedb.org/3/movie/popular?api_key=5a2952737d5c9ffb1a09cbe15f4c142a";

    private static final String TAG = "MainActivity";

    @BindView(R.id.weather_list)
    RecyclerView weatherList;

    @BindView(R.id.section)
    TextView sectionMe;

    @BindView(R.id.favSection)
    LinearLayout sectionFav;

    @BindView(R.id.topSection)
    LinearLayout sectionTop;

    @BindView(R.id.soonSection)
    LinearLayout sectionSoon;

    private List<model_movie> weatherListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        sectionFav.setBackgroundColor(getResources().getColor(R.color.gox_blue));
        sectionSoon.setBackgroundColor(getResources().getColor(R.color.gox_blue));
        sectionTop.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        weatherList.setHasFixedSize(true);
        weatherList.setLayoutManager(new GridLayoutManager(this, 2));

        weatherListData = new ArrayList<model_movie>();

        final WeatherAdapter weatherAdapter = new WeatherAdapter(this, weatherListData);

        //setRecyclerView
        weatherList.setAdapter(weatherAdapter);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlMovieTopRated, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //                Log.d(TAG, "onResponse: "+response);
                for (int x = 0; x < 20; x++) {
                    try {
                        JSONArray results = response.getJSONArray("results");
                        JSONObject object = results.getJSONObject(x);


                        String title = object.getString("title");
                        String language = object.getString("original_language");
                        String rating = object.getString("vote_average");
                        String id = object.getString("id");
                        String count = object.getString("vote_count");
                        String desc = object.getString("overview");
                        String release = object.getString("release_date");
                        String popularity = object.getString("popularity");
                        Log.d(TAG, "Judul Film: " + title);
                        //image
                        String poster = object.getString("poster_path");
                        String backdrop = object.getString("backdrop_path");


                        weatherListData.add(new model_movie(title, language, rating, id, count, desc, release, poster, backdrop, popularity));

                        weatherAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
            }
        });

        Volley.newRequestQueue(getBaseContext()).add(jsonObjectRequest);
    }


        public void fav(View view) {
            sectionFav.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            sectionSoon.setBackgroundColor(getResources().getColor(R.color.gox_blue));
            sectionTop.setBackgroundColor(getResources().getColor(R.color.gox_blue));

            sectionMe.setText("Favorite Section");
            weatherList.setHasFixedSize(true);
            weatherList.setLayoutManager(new GridLayoutManager(this, 2));

            weatherListData = new ArrayList<model_movie>();

            final WeatherAdapter weatherAdapter = new WeatherAdapter(this, weatherListData);

            //setRecyclerView
            weatherList.setAdapter(weatherAdapter);

            final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlMovieFavourite, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //                Log.d(TAG, "onResponse: "+response);
                    for (int x = 0; x < 20; x++) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            JSONObject object = results.getJSONObject(x);


                            String title = object.getString("title");
                            String language = object.getString("original_language");
                            String rating = object.getString("vote_average");
                            String id = object.getString("id");
                            String count = object.getString("vote_count");
                            String desc = object.getString("overview");
                            String release = object.getString("release_date");
                            String popularity = object.getString("popularity");
                            Log.d(TAG, "Judul Film: " + title);
                            //image
                            String poster = object.getString("poster_path");
                            String backdrop = object.getString("backdrop_path");


                            weatherListData.add(new model_movie(title, language, rating, id, count, desc, release, poster, backdrop, popularity));

                            weatherAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: " + error.getMessage());
                }
            });

            Volley.newRequestQueue(getBaseContext()).add(jsonObjectRequest);
        }

    public void top(View view) {

        sectionFav.setBackgroundColor(getResources().getColor(R.color.gox_blue));
        sectionSoon.setBackgroundColor(getResources().getColor(R.color.gox_blue));
        sectionTop.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        sectionMe.setText("Top Movie Section");
        weatherList.setHasFixedSize(true);
        weatherList.setLayoutManager(new GridLayoutManager(this, 2));

        weatherListData = new ArrayList<model_movie>();

        final WeatherAdapter weatherAdapter = new WeatherAdapter(this, weatherListData);

        //setRecyclerView
        weatherList.setAdapter(weatherAdapter);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlMovieTopRated, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //                Log.d(TAG, "onResponse: "+response);
                for (int x = 0; x < 20; x++) {
                    try {
                        JSONArray results = response.getJSONArray("results");
                        JSONObject object = results.getJSONObject(x);


                        String title = object.getString("title");
                        String language = object.getString("original_language");
                        String rating = object.getString("vote_average");
                        String id = object.getString("id");
                        String count = object.getString("vote_count");
                        String desc = object.getString("overview");
                        String release = object.getString("release_date");
                        String popularity = object.getString("popularity");
                        Log.d(TAG, "Judul Film: " + title);
                        //image
                        String poster = object.getString("poster_path");
                        String backdrop = object.getString("backdrop_path");


                        weatherListData.add(new model_movie(title, language, rating, id, count, desc, release, poster, backdrop, popularity));

                        weatherAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
            }
        });

        Volley.newRequestQueue(getBaseContext()).add(jsonObjectRequest);
    }

    public void soon(View view) {
        sectionFav.setBackgroundColor(getResources().getColor(R.color.gox_blue));
        sectionSoon.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        sectionTop.setBackgroundColor(getResources().getColor(R.color.gox_blue));

        sectionMe.setText("Coming Soon Section");
        weatherList.setHasFixedSize(true);
        weatherList.setLayoutManager(new GridLayoutManager(this, 2));

        weatherListData = new ArrayList<model_movie>();

        final WeatherAdapter weatherAdapter = new WeatherAdapter(this, weatherListData);

        //setRecyclerView
        weatherList.setAdapter(weatherAdapter);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlMovieUpComing, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //                Log.d(TAG, "onResponse: "+response);
                for (int x = 0; x < 20; x++) {
                    try {
                        JSONArray results = response.getJSONArray("results");
                        JSONObject object = results.getJSONObject(x);


                        String title = object.getString("title");
                        String language = object.getString("original_language");
                        String rating = object.getString("vote_average");
                        String id = object.getString("id");
                        String count = object.getString("vote_count");
                        String desc = object.getString("overview");
                        String release = object.getString("release_date");
                        String popularity = object.getString("popularity");
                        Log.d(TAG, "Judul Film: " + title);
                        //image
                        String poster = object.getString("poster_path");
                        String backdrop = object.getString("backdrop_path");


                        weatherListData.add(new model_movie(title, language, rating, id, count, desc, release, poster, backdrop, popularity));

                        weatherAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
            }
        });

        Volley.newRequestQueue(getBaseContext()).add(jsonObjectRequest);
    }




}
