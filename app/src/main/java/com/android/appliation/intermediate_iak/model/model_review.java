package com.android.appliation.intermediate_iak.model;

/**
 * Created by adesanto on 11/21/17.
 */

public class model_review {
    private String name, comment;

    public model_review(String name, String comment){
        this.name = name;
        this.comment = comment;
    }

    public String getName(){
        return name;
    }

    public String getComment(){
        return comment;
    }
}
