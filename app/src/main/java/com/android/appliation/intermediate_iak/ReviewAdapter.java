package com.android.appliation.intermediate_iak;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.appliation.intermediate_iak.model.model_movie;
import com.android.appliation.intermediate_iak.model.model_review;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adesanto on 11/21/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder> {
    List<model_review> reviewList;
    private Context context;

    private static final String TAG = "ReviewAdapter";
    public ReviewAdapter(Context context, List<model_review> reviewListData) {
        reviewList = reviewListData;
        this.context = context;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder panjang list: " + reviewList.size());
        View viewContent = LayoutInflater.from(parent.getContext()).inflate(R.layout.review, parent, false);
        return new ReviewViewHolder(viewContent);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        holder.nameAuthor.setText(reviewList.get(position).getName());
        holder.commentAuthor.setText(reviewList.get(position).getComment());
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class ReviewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView nameAuthor;

        @BindView(R.id.comment)
        TextView commentAuthor;

        public ReviewViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
